# Maintainer: Torsten Keßler <tpkessler@archlinux.org>

_srcname=SPIRV-LLVM-Translator
pkgname=${_srcname,,}-14
epoch=1
pkgver=14.0.0.r141+ga883aceb
pkgrel=1
pkgdesc="Tool and a library for bi-directional translation between SPIR-V and LLVM IR (LLVM 14)"
arch=(x86_64)
url="https://github.com/KhronosGroup/SPIRV-LLVM-Translator"
license=(custom)
depends=(llvm14-libs spirv-tools)
makedepends=(git cmake llvm llvm14 spirv-headers) #llvm for lit
checkdepends=(python python-setuptools clang14)
# Current point in the used LLVM 14 branch
_commit=a883acebf1d8015c9fa2c4db9cd8582f2dd14f22
source=(git+${url}.git#commit=$_commit)
sha256sums=('SKIP')

pkgver() {
  cd ${_srcname}
  git describe --tags | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./;s/-/+/'
}

build() {
  cmake -B build -S ${_srcname} \
    -DBUILD_SHARED_LIBS=ON \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr/lib/llvm14 \
    -DCMAKE_PREFIX_PATH=/usr/lib/llvm14/lib/cmake \
    -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
    -DCMAKE_SKIP_RPATH=ON \
    -DLLVM_INCLUDE_TESTS=ON \
    -DLLVM_EXTERNAL_LIT=/usr/bin/lit \
    -DLLVM_EXTERNAL_SPIRV_HEADERS_SOURCE_DIR=/usr/include/spirv/ \
    -Wno-dev
  make -C build
}

check() {
  LD_LIBRARY_PATH="${srcdir}/build/lib/SPIRV" \
  PATH="/usr/lib/llvm14/bin/clang:$PATH" \
  make -C build test
}

package() {
  make -C build DESTDIR="${pkgdir}" install
  install -Dm755 build/tools/llvm-spirv/llvm-spirv -t "${pkgdir}"/usr/bin
  install -Dm644 ${_srcname}/LICENSE.TXT -t "${pkgdir}"/usr/share/licenses/${pkgname}/
}
